<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\General\HomeController;

use Illuminate\Support\Facades\Route;

// General route
Route::group([], function () {
    Route::get('/', [HomeController::class, 'home'])->name('landing');
    Route::get('/aboutus',[HomeController::class, 'aboutUs'])->name('aboutus');
    Route::get('/contactus',[HomeController::class, 'contactUs'])->name('contactus');
    Route::get('/completedprojects',[HomeController::class, 'completedprojects'])->name('completedprojects');

});

// Admin Route
Route::group(['middleware' => ['auth', 'verified', 'admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/dashboard', function () {
         return view('admin.admin_dashboard');
    })->name('dashboard');
});

// Approval Team Route
Route::middleware(['auth', 'verified', 'approvalteam'])->prefix('approvalteam')->group(function () {
    Route::get('/dashboard', function () {
        return view('approvalteam.approvalteam_dashboard');
    })->name('approvalteam.dashboard');

});


// User Route
Route::middleware(['auth', 'verified', 'user'])->prefix('user')->group(function () {
    Route::get('/dashboard', function () {
        return view('user.user_dashboard');
    })->name('user.dashboard');
});


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
