<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id('project_id');
            $table->string('project_title');
            $table->string('prjoect_advisor');
            $table->string('project_manager');
            $table->string('students_required');

            $table->string('company_name');
            $table->string('web_url');
            $table->unsignedBigInteger('phone');            
            $table->string('company_email');
            $table->string('clo');
            $table->string('projectobjectives');

            $table->string('students_number');
            $table->date('duration');
            $table->date('deadline');
            $table->string('project_category');

            $table->string('status')->default("pending");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
