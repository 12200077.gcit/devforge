<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    { 
        Schema::create('students', function (Blueprint $table) {
            $table->string('student_id', 8)->primary(); 
            $table->string('name');
            $table->string('year');
            $table->string('course');
            $table->string('email')->unique();
            $table->string('contact');
            $table->string('linkedIn');
            $table->string('portfolia');
            $table->string('interest');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
