    <!-- NavBar -->
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg bg-body-tertiary" style="background-color: white; height: 78px;" >
            <div class="container-fluid" >
              <a class="navbar-brand" href="{{ url('/') }}">
                  <img src="./images/logo.png" alt="Logo"> <!-- Replace 'your-logo.png' with your actual logo file path -->
                </a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                  <a class="nav-link-about" aria-current="page" href="{{ url('/aboutus') }}">About Us</a>
                  <a class="nav-link-contact" href="{{ url('/contactus') }}">Contact Us</a>
                  <a class="nav-link-projects" href="{{ url('/completedprojects') }}" >Completed Projects</a>
                  @if (Route::has('login'))
                      @auth
                      <form method="POST" action="{{ route('logout') }}">
                        @csrf

                        <a class="btn btn-primary pt-2" href="route('logout')"
                                onclick="event.preventDefault();
                                            this.closest('form').submit();">
                            {{ __('Log Out') }}
                      </a>
                    </form>
                      @else
                      <a href="{{ url('/login') }}" id="loginButton" class="btn btn-primary"><span>Login</span></a>
                      <a href="{{ url('/register') }}" id="signupButton" class="btn btn-success"><span>Signup</span></a>

                      @endauth
                  @endif
                </div>
              </div>
            </div>
          </nav>
    </div>
    <!-- Nav Bar End -->
