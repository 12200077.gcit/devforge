
@extends('layouts.general')
@section('title') Register @endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/register.css') }}">
@endsection
@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6 col-md-8">

        <form id="registerForm" method="POST" action="{{ route('register') }}" autocomplete="off" >
          @csrf
          <div class="mb-3">
            <div class="register-container">
              <span class="register">Register</span>
            </div>
            <div class="input-container">
              <span class="form-label">Enter your details</span>
              <input name="name" type="text" class="form-control  mb-2" id="name" placeholder="Name" required>
              <x-input-error :messages="$errors->get('name')" class="mt-2" />
              <input name="email" type="email" class="form-control  mb-2" id="email" placeholder="Email" required>
              <x-input-error :messages="$errors->get('email')" class="mt-2" />
              <input name="password" type="password" class="form-control mb-2" id="InputPassword" placeholder="Password" required>
              <x-input-error :messages="$errors->get('password')" class="mt-2" />
              <input name="password_confirmation" type="password" class="form-control mb-4" id="confirmPassword" placeholder="Confirm Password" required>
              <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />


              <button type="submit" class="btn btn-info mb-3" id = "registerButton"><span>Register</span></button>

              <div class="login-container">
                <div class="login-link">Already have an account? <a href="{{ url('/login') }}" class="login-link-text">Login</a></div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
