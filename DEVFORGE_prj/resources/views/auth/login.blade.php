
  @extends('layouts.general')
  @section('title') Login @endsection
  @section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}">
  @endsection

 @section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6 col-md-8">
        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />
        <form method="POST" action="{{ route('login') }}">
          @csrf
          <div class="mb-3">
            <div class="login-container">
              <span class="Login">Login</span>
            </div>
            <div class="input-container">
              <span class="form-label">Enter your details</span>
              <input name="email" type="email" class="form-control mt-3 mb-2" id="Inputstudentid" placeholder="Email Address" required autofocus autocomplete="username">
              <x-input-error :messages="$errors->get('email')" class="mt-2" />
              <input type="password" class="form-control mb-2" id="InputPassword" placeholder="Password" name="password" required autocomplete="current-password">
              <x-input-error :messages="$errors->get('password')" class="mt-2" />
              <p class="Forgot-password">Forgot password?</p>
              <button type="submit" class="btn btn-info mb-3"><span>Login</span></button>
              <div class="signup-container">
                <div class="signup-link">Don't have an account? <a href="{{ url('/register') }}" class="signup-link-text">Register</a></div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
