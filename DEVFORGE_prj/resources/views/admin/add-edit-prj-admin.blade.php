<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>director Approval</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        body {
            font-family: 'Inter';
            margin: 0;
            padding: 0;
            font-size: 12px;
        }

        .navbar {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 10px 40px;
            border-bottom: 1px solid #969696;
        }

        .navbar-logo {
            max-width: 90%;
        }

        .navbar-menu {
            display: flex;
            align-items: center;
            flex: 1;
            justify-content: center;
        }

        .navbar-menu-item {
            margin-right: 30px;
            color: #333;
            text-decoration: none;
        }

        .navbar-menu-item:last-child {
            margin-right: 0;
        }

        .navbar-action {
            display: flex;
            align-items: center;
        }

        .navbar-action-text {
            margin-right: 15px;
            font-weight: 100;
            color: #4CAF50;
        }

        .navbar-profile-img {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            object-fit: cover;
            cursor: pointer;
        }

        .navbar-action-button1 {
            padding: 10px 20px;
            border: 1px solid #4CAF50;
            border-radius: 20px;
            cursor: pointer;
            margin-right: 15px;
            position: relative;
        }

        .navbar-action-button2 {
            padding: 10px 20px;
            background-color: #F48422;
            color: white;
            border: none;
            border-radius: 20px;
            cursor: pointer;
            margin-right: 15px;
            position: relative;
        }

        .navbar-action-button3 {
            padding: 10px 20px;
            background-color: #4CAF50;
            color: white;
            border: none;
            border-radius: 20px;
            cursor: pointer;
            margin-right: 15px;
            position: relative;
        }

        .tick-circle {
            width: 15px;
            height: 15px;
            background-color: #FFFFFF;
            border: 1px solid #F48422;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            position: absolute;
            left: 80%;
            bottom: 54%;
            transform: translateY(-50%);
        }

        .tick {
            color: #F48422;
            font-size: 10px;
        }

        .rejected {
            color: #FF8989;
        }

        .approved {
            color: #74B444;
        }

        .end-button {
            margin-left: 77%;
        }

        .clear-form-button {
            padding: 6px 20px 6px 20px;
            color: #74B444;
            border: 1px solid #74B444;
            cursor: pointer;
            border-radius: 20px;
            background-color: transparent;
            box-shadow: none;
            margin-bottom: 10%;
        }

        .add-project-button {
            padding: 6px 20px 6px 20px;
            color: #fff;
            border: none;
            border-radius: 20px;
            cursor: pointer;
            background-color: #74B444;
            box-shadow: none;
            margin-bottom: 10%;
            margin-left: 15px;
        }

        .container {
            display: flex;
        }

        .form-container {
            flex: 1;
            padding: 10px;
        }

        .project-container {
            flex: 1;
            padding: 20px;


            .input-field {
                width: 28%;
                margin-bottom: 10px;
                margin-right: 60%;
                padding: 8px;
                border-radius: 20px;
            }
        }

        .input-field {
            width: 90%;
            margin-bottom: 10px;
            padding: 8px;
            border-radius: 20px;
        }

        .textarea-field {
            width: 100%;
            height: fit-content;
            margin-bottom: 10px;
            padding: 8px;
            height: 80%;
            border-radius: 20px;
        }

        .error-message {
            color: red;
            font-size: 12px;
            margin-top: 5px;
        }
    </style>
</head>

<body>
@include('admin.adminNavbar')

    <div class="container">
        <div class="form-container">
            <h4>Project Overview</h4>
            <input type="text" class="input-field" id="project-title" placeholder="Project Title" required>
            <input type="text" class="input-field" id="project-advisor" placeholder="Project Advisor" required>
            <input type="text" class="input-field" id="project-manager" placeholder="Project Manager" required>
            <input type="number" class="input-field" id="no-of-students" placeholder="No of students" required>
               
        </div>

        <div class="form-container">
            <h4>Client Details</h4>
            <input type="text" class="input-field" id="company-name" placeholder="Company Name" required>
            <input type="url" class="input-field" id="web-url" placeholder="Web URL" required>
            <input type="number" class="input-field" id="telephone-mobile" placeholder="Telephone/Mobile" pattern="\+97517\d{6}" required>
            <input type="email" class="input-field" id="email" placeholder="Email" required>
            <input type="text" class="input-field" id="clo" placeholder="Company Liason Officer (CLO)" required>
        </div>
        <div class="form-container">
            <h4>Project Objectives</h4>
            <textarea class="textarea-field" id="project-objectives" placeholder="Project Objectives" rows="4"
                required></textarea>
        </div>
    </div>
    <div class="container">
        <div class="project-container">
            <h4>Project Details</h4>
            <input type="number" class="input-field" id="no-of-student" placeholder="No of student" required>
            <input type="text" class="input-field" id="duration" placeholder="Duration" required>
            <input type="date" class="input-field" id="deadline" placeholder="Deadline" required>
            <input type="text" class="input-field" id="project-category" placeholder="Project Category" required>
        </div>
    </div>
    <div class="end-button">
        <button class="clear-form-button" onclick="clearForm()">clear form</button>
        <button class="add-project-button" onclick="addProject()" disabled>Add Project</button>
    </div>

    <script>
        function clearForm() {
            var inputs = document.querySelectorAll('.input-field');
            var textarea = document.querySelector('.textarea-field');

            inputs.forEach(function (input) {
                input.value = '';
                input.classList.remove('error');
                input.nextElementSibling.textContent = '';
            });

            textarea.value = '';
            textarea.classList.remove('error');
            textarea.nextElementSibling.textContent = '';

            disableAddProjectButton();
        }

        function addProject() {
            alert('Project has been successfully added.');
        }

        function disableAddProjectButton() {
            var inputs = document.querySelectorAll('.input-field');
            var textarea = document.querySelector('.textarea-field');
            var addButton = document.querySelector('.add-project-button');

            var allFilled = true;
            var validEmail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
            var validPhone = /^\+97517\d{6}$/;

            inputs.forEach(function (input) {
                if (input.value === '') {
                    allFilled = false;
                    input.classList.add('error');
                    input.nextElementSibling.textContent = 'This field is required';
                } else if (input.getAttribute('type') === 'email' && !validEmail.test(input.value)) {
                    allFilled = false;
                    input.classList.add('error');
                    input.nextElementSibling.textContent = 'Please enter a valid email address';
                } else if (input.getAttribute('type') === 'tel' && !validPhone.test(input.value)) {
                    allFilled = false;
                    input.classList.add('error');
                    input.nextElementSibling.textContent = 'Please enter a valid telephone/mobile number (e.g., +97517XXXXXXXX)';
                } else {
                    input.classList.remove('error');
                    input.nextElementSibling.textContent = '';
                }
            });

            if (textarea.value === '') {
                allFilled = false;
                textarea.classList.add('error');
                textarea.nextElementSibling.textContent = 'This field is required';
            } else {
                textarea.classList.remove('error');
                textarea.nextElementSibling.textContent = '';
            }

            if (allFilled) {
                addButton.disabled = false;
            } else {
                addButton.disabled = true;
            }
        }
    </script>

</body>

</html> 





