<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
    <title>Supervisor Overview</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/supervisorOverview.css') }}">
    
</head>


<body>
    @include('admin.adminNavbar')
      <div class="container my-4">
        
        <div class="nav nav-tabs container" id="nav-tab" role="tablist">
          
            <button class="nav-link active" id="nav-overview-tab" data-bs-toggle="tab" data-bs-target="#nav-overview"  
            type="button" role="tab" aria-controls="nav-overview" 
            aria-selected="true" >Overview</button>

            <button class="nav-link" id="nav-ongoing-projects-tab" data-bs-toggle="tab" data-bs-target="#nav-ongoing-projects"  
            type="button" role="tab" aria-controls="nav-ongoing-projects" 
            aria-selected="false" >Ongoing Projects</button>
          
            <button class="nav-link" id="nav-project-deadline-tab" data-bs-toggle="tab" data-bs-target="#nav-project-deadline"  
            type="button" role="tab" aria-controls="nav-project-deadline" 
            aria-selected="false" >Projects Close to Deadline</button>
            
            </div>
       

    <div class="tab-content border g-0 rounded border-top-0 custom-tab-content" id ="nav-tabContent">

        <div id="nav-overview" class="tab-pane show active p-3" role="tabpanel" aria-labelledby="nav-overview-tab">
           
        <div class="row">
        <div class="col col-auto d-flex justify-content-center align-items-center" >
            <!-- First container -->
            <div class="d-flex flex-column align-items-center chart-container">
                <!-- Content for the first container -->
                
                <!-- Canvas for the pie chart -->
                <canvas id="myPieChart" width="100" height="100"></canvas>
            <!-- Legend for the pie chart -->
            <div id="legend"  class="mt-5"></div>
            </div>
        </div>

            

        <div class="col col-auto">
            <!-- Second container -->
            <div class="resourse-container">
                <!-- Content for the second container -->
                <div>
                    <h5 class="text">Resource Planning</h5>
                    <div class="text-container">
                        <div class="text-left">
                            <p class="top-left-no">50</p>
                            <p class="split-text">Students working<br> on projects</p>
                        </div>
                    <div class="text-right">
                        <p class="top-right-no">10</p>
                        <p class="split-text">Students Available <br> for projects</p>
                    </div>
                </div>
            </div>
            
            </div>
            <!-- end text content -->

            <!-- search field start -->
            <div class="input-group mt-3 mb-3">
                <input type="text" class="form-control rounded-pill py-2" 
                    placeholder="Search name" aria-label="Username" aria-describedby="addon-wrapping">
            </div>

            <table class="first table">
                <thead>
                    <tr>
                    <th scope="col">Student Name</th>
                    <th scope="col">Year</th>
                    <th scope="col">Project in Progress</th>
                    <th scope="col">Project Completed</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th class ="g-c" scope="row">Sangay Tshering</th>
                    <td>4</td>
                    <td>2</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <th  class ="g-c" scope="row">Deki Yangden</th>
                    <td>3</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <th  class ="g-c" scope="row">Sonam Yeshi</th>
                    <td>2</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <th  class ="g-c" scope="row">Karma Yeshi</th>
                    <td>2</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <th  class ="g-c" scope="row">Pema Wangmo</th>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    </tr>
                </tbody>
            </table>
            <!-- end table -->

            <div class="text-center">
                <a class="link-offset-2 link-underline link-underline-opacity-0" href="#">View more</a>
            </div>

            <!-- this is last div for the column -->
        </div>
        <!-- end 2nd column -->
        
           
        <!-- start of 3rd column -->
        <div class="col">
            <!-- Third container -->
            <div class="container">
                <!-- Content for the third container -->
                <h5>Lists of Projects Close to Deadline</h5>

                <!-- start table -->
                <table class="table table-borderless">
                <thead>
                    <tr>
                    <th scope="col">Number of days left</th>
                    <th scope="col">Project name</th>
                    <th scope="col">Deadline</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">50</th>
                    <td>Hotel Reservation System</td>
                    <td>March 22, 2024</td>
                    </tr>
                    <tr>
                    <th scope="row">20</th>
                    <td>Hopital Management System</td>
                    <td>May 30, 2024</td>
                    </tr>
                    <tr>
                    <th scope="row">30</th>
                    <td>Mess Management system</td>
                    <td>May 18, 2024</td>
                    </tr>
                    <tr>
                    <th scope="row">10</th>
                    <td>Project Management System</td>
                    <td>May 30, 2024</td>
                    </tr>
                </tbody>
            </table>
            <!-- End table -->

            <div class="text-center">
                <p><a class="link-offset-2 link-underline link-underline-opacity-0" id="viewMoreLink">View more</a></p>
            </div>
        </div>
       
        </div>   <!-- last div for row -->

       
    </div>
        
</div> <!--end for the overview container  -->




        <!--ongoing-projects  -->
        <div id="nav-ongoing-projects" class="tab-pane fade custom-project-container" 
        role="tabpanel" aria-labelledby="nav-ongoing-projects-tab">
        <h4 class="text-center mb-4">Resourse Planning</h4>

        <div class="container">
        <div class="row"><!-- Row for content -->
                <div class="col-md-4"><!-- Column for left text -->
                    <div class="text-left">
                        <p class="top-left-no">50</p>
                        <p class="split-text">Students working <br> on projects</p>
                    </div>
                </div><!-- End left text column -->
                
                <div class="col-md-4" style="margin-top: 90px;"><!-- Column for search container -->
                    <div class="input-group">
                        <input type="text" class="form-control rounded-pill py-2 px-5" 
                            placeholder="Search name" aria-label="Username" aria-describedby="addon-wrapping">
                    </div>
                </div><!-- End search container column -->
                
                <div class="col-md-4"><!-- Column for right text -->
                    <div class="text-right">
                        <p class="top-right-no">10</p>
                        <p class="split-text">Students Available <br> for projects</p>
                    </div>
                </div><!-- End right text column -->
        </div><!-- End content row -->
        </div><!-- End container -->

           
        <div class="container mt-3">
              <!-- start table -->
            <div class="table-container d-flex justify-content-center mt-3" id="resourseTable">
            <table class="table custom-table-width smaller-table">
                    <thead>
                        <tr>
                        <th scope="col" class="col-sm-3" >Student Name</th>
                        <th scope="col"  class="col-sm-2">Year</th>
                        <th scope="col"  class="col-sm-2">Project in Progress</th>
                        <th scope="col"  class="col-sm-2">Project Completed</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">Sangay Tshering</th>
                        <td>4</td>
                        <td>2</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <th scope="row">Deki Yangden</th>
                        <td>3</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <th scope="row">Sonam Yeshi</th>
                        <td>2</td>
                        <td>1</td>
                        <td>2</td>
                        </tr>
                        <tr>
                        <th scope="row">Karma Yeshi</th>
                        <td>2</td>
                        <td>1</td>
                        <td>1</td>
                        </tr>
                        <tr>
                        <th scope="row">Pema Wangmo</th>
                        <td>1</td>
                        <td>1</td>
                        <td>0</td>
                        </tr>
                        <tr>
                        <th scope="row">Pema Wangmo</th>
                        <td>1</td>
                        <td>1</td>
                        <td>0</td>
                        </tr>
                        <tr>
                        <th scope="row">Pema Wangmo</th>
                        <td>1</td>
                        <td>1</td>
                        <td>0</td>
                        </tr>
                    </tbody>
                </table>
            </div>
                <!-- end table -->

        </div>
        

            <div class="text-center">
                <p><a class="link-offset-2 link-underline link-underline-opacity-0" href="#">View less</a></p>
            </div>

        </div><!-- End ongoing-projects -->


        

        <div id="nav-project-deadline" class="tab-pane fade active p-3" role="tabpanel" aria-labelledby="nav-project-deadline-tab">
            <span class="Overview">Overview for Container 3</span>
            
            <table class="table">
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>Student Name</th>
                        <th>Year</th>
                        <th>Supervisor</th>
                        <th>Deadline</th>
                        <th>Task</th>
                        <th>Scoping Document</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>FYP</td>
                        <td>Sangay Tshering</td>
                        <td>4</td>
                        <td>Karma Dorji</td>
                        <td>April 26, 2024</td>
                        <td>Requirement gathering</td>
                        <td class="approved">Download</td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>FYP</td>
                        <td>Deki lhazom</td>
                        <td>3</td>
                        <td>Karma Dorji</td>
                        <td>April 26, 2024</td>
                        <td>Design</td>
                        <td class="approved">Download</td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>FYP</td>
                        <td>Sonam Wangchuk</td>
                        <td>4</td>
                        <td>Karma Dorji</td>
                        <td>April 26, 2024</td>
                        <td>Requirement gathering</td>
                        <td class="approved">Download</td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>Gyalsung Project</td>
                        <td>Sonam Wangchuk</td>
                        <td>4</td>
                        <td>Karma Dorji</td>
                        <td>April 26, 2024</td>
                        <td>Requirement gathering</td>
                        <td class="approved">Download</td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>gyalsung project</td>
                        <td>Deki</td>
                        <td>4</td>
                        <td>Karma Dorji</td>
                        <td>April 26, 2024</td>
                        <td>Requirement gathering</td>
                        <td class="approved">Download</td>
                    </tr>
                </tbody>
            </table>
          
    
    
        </div><!-- overview end -->

    </div><!-- common contanier end -->
   
    

    
    




    </div><!-- End tab -->

  

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Your custom JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
  

    <script>
    // JavaScript to create a pie chart using Chart.js
    var ctx = document.getElementById('myPieChart').getContext('2d');
    var myPieChart = new Chart(ctx, {
        type: 'doughnut', // Use doughnut type for hollow center
        data: {
            labels: ['Open Projects', 'Closed Projects', 'In-progress'],
            datasets: [{
                data: [30, 40, 30], // Adjust the data values as needed
                backgroundColor: ['#9747FF', '#74B444', '#F48422'], // Set colors
            }]
        },
        options: {
            cutoutPercentage: 10, // Adjust the cutout percentage to make the center hollow
            legend: {
                display: false
            },
            plugins: {
                legend: {
                    display: false
                },
                tooltip: {
                    enabled: false
                }
            },
            elements: {
                arc: {
                    borderWidth: 0
                }
            }
        }
    });

    // Custom legend
    var legend = document.getElementById('legend');
    var labels = myPieChart.data.labels;
    var backgroundColors = myPieChart.data.datasets[0].backgroundColor;
    var values = myPieChart.data.datasets[0].data;
    for (var i = 0; i < labels.length; i++) {
        var label = document.createElement('div');
        label.innerHTML = '<span style="display:inline-block;width:10px;height:10px;background-color:' + backgroundColors[i] + '"></span> ' + labels[i] + ': ' + values[i];
        legend.appendChild(label);
    }
</script>



<script>
    document.getElementById('viewMoreLink').addEventListener('click', function(event) {
        event.preventDefault(); // Prevent default link behavior
        var tabButton = document.getElementById('nav-ongoing-projects-tab');
        if (tabButton) {
            tabButton.click(); // Trigger click event on the tab button
        }
    });
</script>
   
</body>

</html>
