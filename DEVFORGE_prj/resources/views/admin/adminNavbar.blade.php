

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Navbar after login</title>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>

<style>
    .container-fluid {
        display: flex;
        justify-content: space-between;
        align-items: center;
        }
    .navbar {
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 10px 40px;
        border-bottom: 1px solid #969696;
    }
    .navbar-nav {
    display: flex;
    align-items: center;
    list-style: none; /* Remove default list styles */
    padding: 0; /* Remove default padding */   
    }
    .navbar-nav .nav-item {
        margin-right: 20px; /* Adjust spacing between nav items */
    }

    .navbar-logo {
        max-width: 90%;
    }

   /* Styling for the navigation links */
    .nav-link {
    color: #676767;
    margin: 0 20px; 
    text-decoration: none;
    font-size: 16px;
    font-weight: 500;
    font-family: Inter;
    transition: color 0.3s; 
    }

    .nav-link:hover {
    color: #74b444;
    text-decoration: none;

    }
    .navbar-action {
        display: flex;
        align-items: center;
    }
    .navbar-action-text {
        margin-right: 15px; 
        font-weight: 100; 
        color: #4CAF50;
    }
    .navbar-profile-img {
        width: 40px;
        height: 40px;
        border-radius: 50%; /* Make the image round */
        object-fit: cover;
        cursor: pointer; /* Add cursor pointer */
    }
    
    .navbar-action-button {
        padding: 10px 20px;
        background-color: #F48422;
        color: white;
        border: none;
        border-radius: 20px; 
        cursor: pointer;
        margin-right: 15px; 
        position: relative; 
    }
    
</style>
</head>

<body>  

</div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <!-- Navbar brand and toggle button -->
            <a class="navbar-brand" href="#">
                <img src="{{ asset('images/logo.png') }}" alt="Logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" 
            data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" 
            aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Completed Projects</a>
                    </li>
                    
                    
                </ul>
            </div>

            <div class="d-flex align-items-center">
                <button class="navbar-action-button" data-toggle="modal" onclick="redirectApprovalPage()">Approve Project</button>
                <button class="navbar-action-button" data-toggle="modal" onclick="redirectAddEditPrj()">Add/Edit Project</button>
                <div class="navbar-action-text">Hi AdminXX</div>
                <img src="{{ asset('images/profile.png') }}" alt="Profile" class="navbar-profile-img" data-toggle="modal"
                    data-target="#profileModal">
        </div>
    </div>
    </nav>
    <!-- Navbar end -->

<script>
    function redirectApprovalPage() {
        window.location.href = "{{ route('admin_approval') }}";
    }
</script>

<script>
    function redirectAddEditPrj() {
        window.location.href = "{{ route('add_edit_prj_admin') }}";
    }
</script>
    
    
</body>
</html>