<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>director Approval</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
    body {
        font-family: 'Inter'; 
        margin: 0;
        padding: 0;
        font-size: 12px; /* Reduced font size */
    }
    .navbar {
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 10px 40px;
        border-bottom: 1px solid #969696;
    }
    .navbar-logo {
        max-width: 90%;
    }
    .navbar-menu {
        display: flex;
        align-items: center;
        flex: 1; 
        justify-content: center; /* Center aligning the menu items */
    }
    .navbar-menu-item {
        margin-right: 30px;
        color: #333; /* Changed color to dark gray */
        text-decoration: none;
    }
    .navbar-menu-item:last-child {
        margin-right: 0;
    }
    .navbar-action {
        display: flex;
        align-items: center;
    }
    .navbar-action-text {
        margin-right: 15px; 
        font-weight: 100; 
        color: #4CAF50;
    }
    .navbar-profile-img {
        width: 40px;
        height: 40px;
        border-radius: 50%; /* Make the image round */
        object-fit: cover;
        cursor: pointer; /* Add cursor pointer */
    }
    .navbar-action-button1 {
        padding: 10px 20px;
        
        border: 1px solid #4CAF50 ;
        border-radius: 20px; 
        cursor: pointer;
        margin-right: 15px; 
        position: relative; 
    }
    .navbar-action-button2 {
        padding: 10px 20px;
        background-color: #F48422;
        color: white;
        border: none;
        border-radius: 20px; 
        cursor: pointer;
        margin-right: 15px; 
        position: relative; 
    }
    .navbar-action-button3 {
        padding: 10px 20px;
        background-color: #4CAF50 ;
        color: white;
        border: none;
        border-radius: 20px; 
        cursor: pointer;
        margin-right: 15px; 
        position: relative; 
    }
    .tick-circle {
        width: 15px;
        height: 15px;
        background-color: #FFFFFF;
        border:  1px solid #F48422;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        position: absolute;
        left: 80%;
        bottom: 54%;
        transform: translateY(-50%);
    }
    .tick {
        color: #F48422;
        font-size: 10px;
    }
    .rejected {
        color:#FF8989;
    }
    .approved {
        color: #74B444;
    }
   

     .reject-button {
        padding: 6px 20px 6px 20px;
        border-radius: 20px;
        background-color: #F48422;
        color: white;
        border: none;
        cursor: pointer;
      
    }

    
    .approve-button {
        padding: 6px 20px 6px 20px;
        border-radius: 20px;
        background-color: #74B444;
        color: white;
        border: none;
        cursor: pointer;
      
    }
  
   
</style>
</head>
<body>
@include('admin.adminNavbar')


    
    <div class="container">
        <h4>Projects for Approval(Director)</h4>

            <table class="table">
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>Scoping Document</th>
                        <th style="padding-left: 6%;">Status</th>
                        <th>action</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>FYP</td>
                        <td class="approved">See scoping document</td>
                        <td >
                            <button class="reject-button">Reject</button>
                            <button class="approve-button">Approve</button>
                        </td>
                        <td class="approved">Feedback</td>
                    </tr>
    

                    <tr>
                        <td>Gyalsung Project</td>
                        <td class="approved">See scoping document</td>
                        <td class="rejected">Rejected</td>
                        <td class="approved">Feedback</td>
                    </tr>

                    <tr>
                        <td>Hotel Management System</td>
                        <td class="approved">See scoping document</td>
                        <td class="approved">Approved</td>
                        <td class="approved">Feedback</td>
                    </tr>

                    <tr>
                        <td>FYP</td>
                        <td class="approved">See scoping document</td>
                        <td >
                            <button class="reject-button">Reject</button>
                            <button class="approve-button">Approve</button>
                        </td>
                        <td class="approved">Feedback</td>
                    </tr>
                    
                    
                </tbody>
            </table>
          </div>
        </div>


    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
        });

        function uploadFile() {
            $('#fileInput').click();
        }

        function uploadMom() {
            // Implement file upload for Minutes of Meeting
            $('#fileInput').click();
        }
    </script>

    <!-- File input element (hidden) -->
    <input type="file" id="fileInput" style="display: none;">
</body>
</html>
