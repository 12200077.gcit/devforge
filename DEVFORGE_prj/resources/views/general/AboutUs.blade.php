@extends('layouts.general')
    @section('title') Contact Us Page @endsection
    <style>
     .overallContainer {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
    }
    .contact-container {
        width: 30%;
        margin: 20px 40px;
        padding: 20px;
    }
    .graduates-container{
        width: 60%;
        margin: 20px 40px;
        padding: 20px;
    }
    h3 {
        font-weight: bold;
        margin-top: -10px;
    }
    p {
        font-size: 14px;
    }
    .card-container {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        margin-top: -50px;
        margin-left: 40px;
        padding: 20px;
    }
    .card {
        border: 1px solid #ccc;
        border-radius: 10px;
        padding: 20px;
        width: calc(15% - 10px);
        margin-bottom: 20px;
        box-sizing: border-box;
    }
    .card img {
        max-width: 100%; /* Ensures the image fits within the card */
        height: auto; /* Maintains aspect ratio */
        border-radius: 8px; /* Rounded corners */
    }

    </style>
   @section('content')
    <div class="overallContainer">
    <div class="contact-container">
        <h4>What DevForge Is</h4>
        <p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</p>

        <h4>Current Students in DevForge</h4>
        <p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</p>


    </div>

    <div class="card-container">
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">

        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">

        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>

    </div>
    <div class="graduates-container">

        <h4>Graduates DevForge</h4>
        <p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</p>
    </div>


    <div class="card-container">
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">

        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">

        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>

        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
    </div>
    <div class="card-container">
        <div class="card">
            <<img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">

        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">

        </div>
        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>

        <div class="card">
        <img src="{{ asset('images/img.jpeg') }}" alt="Your Image">
        </div>
    </div>
</div>
@endsection
