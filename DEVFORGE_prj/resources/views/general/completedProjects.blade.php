@extends('layouts.general')
    @section('title') Completed Project @endsection
@section('content')
  <h1>Complted Project</h1>
  <div class="all">
    <div class="container">
        <h3>Completed Projects</h3>
        <div class="button-group">
            <button class="green-button">All</button>
            <button>Website</button>
            <button>Mobile App</button>
        </div>
    </div>
    <div class="container">
        <div class="white-background" id="projectDetailsContainer">
            <span class="close" onclick="closeProjectDetails()">&times;</span>
            <div class="card-inside">
                <div class="project-details">
                    <h4 class="project-title">Project Name 1</h4>
                    <p> Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
                        <br><br>
                        Date completed: MM/DD/YYY <br>
                        <br />
                        Project Team members:<br><br>
                        Team member A <br>
                        Team member B <br>
                        Team member C <br>
                        Team member D <br>
                        Team member E
                    </p>
                </div>
                <div class="image">
                    <img src="{{asset('images/Rectangle 77.png')}}" alt="Project Image" class="project-image">
                </div>
            </div>
        </div> <!-- White background div -->
        <div class="card-container">
            <div class="card">
                <h4>Project Name 1</h4>
                <div class="tick-circle">
                    <div class="tick">1</div>
                </div>
                <p>Description of Project 1</p>
                <p>Students: Sangay Tshering, Deki, Phuntsho</p>
                <button class="green-button" onclick="showProjectDetails()">View Project</button>
            </div>
            <!-- Add other cards here -->
            <div class="card">
                <h4>Project Name 2</h4>
                <div class="tick-circle">
                    <div class="tick">1</div>
                </div>
                <p>Description of Project 2</p>
                <p>Students: Sangay Tshering, Deki, Phuntsho</p>
                <button class="green-button" onclick="showProjectDetails()">View Project</button>
            </div>
            <div class="card">
                <h4>Project Name 3</h4>
                <div class="tick-circle">
                    <div class="tick">1</div>
                </div>
                <p>Description of Project 3</p>
                <p>Students: Sangay Tshering, Deki, Phuntsho</p>
                <button class="green-button" onclick="showProjectDetails()">View Project</button>
            </div>
            <div class="card">
                <h4>Project Name 4</h4>
                <div class="tick-circle">
                    <div class="tick">1</div>
                </div>
                <p>Description of Project 4</p>
                <p>Students: Sangay Tshering, Deki, Phuntsho</p>
                <button class="green-button" onclick="showProjectDetails()">View Project</button>
            </div>
        </div>
        <div class="card-container">
            <!-- Add cards here -->
        </div>

        <div class="card-container">
            <div class="card">
                <h4>Project Name 1</h4>
                <div class="tick-circle">
                    <div class="tick">1</div>
                </div>
                <p>Description of Project 1</p>
                <p>Students: Sangay Tshering, Deki, Phuntsho</p>
                <button class="green-button" onclick="showProjectDetails()">View Project</button>
            </div>
            <div class="card">
                <h4>Project Name 2</h4>
                <div class="tick-circle">
                    <div class="tick">1</div>
                </div>
                <p>Description of Project 2</p>
                <p>Students: Sangay Tshering, Deki, Phuntsho</p>
                <button class="green-button" onclick="showProjectDetails()">View Project</button>
            </div>
            <div class="card">
                <h4>Project Name 3</h4>
                <div class="tick-circle">
                    <div class="tick">1</div>
                </div>
                <p>Description of Project 3</p>
                <p>Students: Sangay Tshering, Deki, Phuntsho</p>
                <button class="green-button" onclick="showProjectDetails()">View Project</button>
            </div>
            <div class="card">
                <h4>Project Name 4</h4>
                <div class="tick-circle">
                    <div class="tick">1</div>
                </div>
                <p>Description of Project 4</p>
                <p>Students: Sangay Tshering, Deki, Phuntsho</p>
                <button class="green-button" onclick="showProjectDetails()">View Project</button>
            </div>
        </div>
        <p class="loadmore">Load more project</p>
    </div>
</div>

<script>
    function showProjectDetails() {

        document.getElementById("projectDetailsContainer").style.display = "block";
    }

    function closeProjectDetails() {

        document.getElementById("projectDetailsContainer").style.display = "none";
    }
</script>
@endsection
