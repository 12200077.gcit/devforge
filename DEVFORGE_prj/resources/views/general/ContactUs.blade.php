@extends('layouts.general')
    @section('title') Contact Us Page @endsection

<style>
    .overallContainer {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        .contact-container {
            width: 30%;
            margin: 20px 40px;
            padding: 20px;
            float: left;
        }

        h1 {
            font-weight: bold;
        }

        input[type="text"],
        textarea,
        button {
            width: calc(100% - 20px);
            padding: 15px;
            margin-top: 10px;
            border-radius: 40px;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        textarea {
            height: 100px;
            border-radius: 20px;
            /* Reduced border radius for textarea */
        }

        button {
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #45a049;
        }

        p {
            font-size: 14px;
        }

</style>
@section('content')
{{-- @include('user.navBarafterlogin') --}}
    <div class="overallContainer">
        <div class="contact-container">
            <h3>Talk to Us</h3>
            <p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.!</p>
            <form action="#" method="post">
                <input class="mb-2" type="text" name="name" placeholder="Your Name">
                <input class="mb-2" type="text" name="email" placeholder="Your Email">
                <textarea class="mb-3" name="message" placeholder="Your Message"></textarea>
                <button type="submit">Send Message</button>
            </form>
        </div>
    </div>
@endsection
