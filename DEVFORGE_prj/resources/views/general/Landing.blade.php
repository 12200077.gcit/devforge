@extends('layouts.general')
    @section('title') Home Page @endsection
    @section('css')
    <link rel="stylesheet" href="{{ asset('css/homepage.css')}}">
    @endsection
   @section('content')
    <!-- Hero Banner -->
    <div class="hero-banner">
        <img src="./images/herobanner.png" class="img-fluid" alt="Hero Image">

        <div class="hero-text text-center text-white">
            <h1 class="display-4">What is it for?</h1>
            <p class="lead">publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</p>
        </div>

    </div>
    <!-- Hero Banner End -->
    <!-- Main Page -->
        <h1 class="project-title">Available Projects</h1>

        <div class="container custom-container">
          <div class="row">
              <div class="col-md-1">
                  <button type="button" class="btn btn-secondary" id="allButton"><span>All</span></button>
              </div>
              <div class="col-md-1">
                  <button type="button" class="btn btn-danger"  id="websiteButton"><span>Website</span></button>
              </div>
              <div class="col-md-1">
                  <button type="button" class="btn btn-warning"  id="mobileAppButton"><span>Mobile App</span></button>
              </div>
          </div>
      </div>


      <!-- Cards -->
      <div class="card-container">
        <!-- row 1 -->
        <div class="row">
            <div class="col-md-3">
                <div class="card custom-card">
                    <div class="card-body">
                      <!-- rectangle -->
                      <div class="Rectangle">
                        <div class="deadline">
                          <p>Closing soon April 24</p>
                      </div>
                    </div>
                      <!-- end rectangle -->
                        <h5 class="card-title">Project Name can put here</h5>
                        <p class="card-text">In publishing and graphic design,
                          Lorem ipsum is a placeholder text commonly used to
                          demonstrate the visual form of a document or a typeface
                          without relying on meaningful content.n publishing and
                          graphic design, Lorem ipsum is a placeholder text commonly
                           used to demonstrate.</p>
                          <!-- card buttons -->
                           <div class="button-container">
                            <div class="row">
                                <div class="col-md-5">
                                    <button type="button" class="btn btn-outline-primary">
                                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                      </svg>
                                      <span>students</span>
                                    </button>
                                </div>
                                <div class="col-md-5">
                                    <button type="button" class="btn btn-outline-primary">
                                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                        <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                        <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                      </svg>
                                      <span>2 months</span>
                                    </button>
                                </div>
                                <div class="col-md-5">
                                    <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                                </div>
                            </div>
                        </div>
                        <!-- end card buttons -->
                        <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card custom-card">
                    <div class="card-body">
                      <!-- rectangle -->
                      <div class="Rectangle">
                        <div class="deadline">
                          <p>Closing soon April 24</p>
                      </div>
                    </div>
                      <!-- end rectangle -->
                      <h5 class="card-title">Project Name can put here</h5>
                      <p class="card-text">In publishing and graphic design,
                        Lorem ipsum is a placeholder text commonly used to
                        demonstrate the visual form of a document or a typeface
                        without relying on meaningful content.n publishing and
                        graphic design, Lorem ipsum is a placeholder text commonly
                         used to demonstrate.</p>
                        <!-- card buttons -->
                         <div class="button-container">
                          <div class="row">
                              <div class="col-md-5">
                                  <button type="button" class="btn btn-outline-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                      <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                    </svg>
                                    <span>students</span>
                                  </button>
                              </div>
                              <div class="col-md-5">
                                  <button type="button" class="btn btn-outline-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                      <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                      <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                    </svg>
                                    <span>2 months</span>
                                  </button>
                              </div>
                              <div class="col-md-5">
                                  <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                              </div>
                          </div>
                      </div>
                      <!-- end card buttons -->
                      <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                    </div>
                </div>
            </div>

            <div class="col-md-3  ">
                <div class="card custom-card">
                    <div class="card-body">
                      <!-- rectangle -->
                      <div class="Rectangle">
                        <div class="deadline">
                          <p>Closing soon April 24</p>
                      </div>
                    </div>
                      <!-- end rectangle -->
                      <h5 class="card-title">Project Name can put here</h5>
                      <p class="card-text">In publishing and graphic design,
                        Lorem ipsum is a placeholder text commonly used to
                        demonstrate the visual form of a document or a typeface
                        without relying on meaningful content.n publishing and
                        graphic design, Lorem ipsum is a placeholder text commonly
                         used to demonstrate.</p>
                        <!-- card buttons -->
                         <div class="button-container">
                          <div class="row">
                              <div class="col-md-5">
                                  <button type="button" class="btn btn-outline-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                      <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                    </svg>
                                    <span>students</span>
                                  </button>
                              </div>
                              <div class="col-md-5">
                                  <button type="button" class="btn btn-outline-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                      <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                      <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                    </svg>
                                    <span>2 months</span>
                                  </button>
                              </div>
                              <div class="col-md-5">
                                  <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                              </div>
                          </div>
                      </div>
                      <!-- end card buttons -->
                      <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                    </div>
                </div>
            </div>

            <div class="col-md-3  ">
                <div class="card custom-card">
                    <div class="card-body">
                      <!-- rectangle -->
                      <div class="Rectangle">
                        <div class="deadline">
                          <p>Closing soon April 24</p>
                      </div>
                    </div>
                      <!-- end rectangle -->
                      <h5 class="card-title">Project Name can put here</h5>
                      <p class="card-text">In publishing and graphic design,
                        Lorem ipsum is a placeholder text commonly used to
                        demonstrate the visual form of a document or a typeface
                        without relying on meaningful content.n publishing and
                        graphic design, Lorem ipsum is a placeholder text commonly
                         used to demonstrate.</p>
                        <!-- card buttons -->
                         <div class="button-container">
                          <div class="row">
                              <div class="col-md-5">
                                  <button type="button" class="btn btn-outline-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                      <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                    </svg>
                                    <span>students</span>
                                  </button>
                              </div>
                              <div class="col-md-5">
                                  <button type="button" class="btn btn-outline-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                      <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                      <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                    </svg>
                                    <span>2 months</span>
                                  </button>
                              </div>
                              <div class="col-md-5">
                                  <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                              </div>
                          </div>
                      </div>
                      <!-- end card buttons -->
                      <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row 1 -->

        <!-- row 2 -->
        <div class="row">
          <div class="col-md-3">
              <div class="card custom-card">
                  <div class="card-body">
                    <!-- rectangle -->
                    <div class="Rectangle">
                      <div class="deadline">
                        <p>Closing soon April 24</p>
                    </div>
                  </div>
                    <!-- end rectangle -->
                    <h5 class="card-title">Project Name can put here</h5>
                    <p class="card-text">In publishing and graphic design,
                      Lorem ipsum is a placeholder text commonly used to
                      demonstrate the visual form of a document or a typeface
                      without relying on meaningful content.n publishing and
                      graphic design, Lorem ipsum is a placeholder text commonly
                       used to demonstrate.</p>
                      <!-- card buttons -->
                       <div class="button-container">
                        <div class="row">
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                    <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                  </svg>
                                  <span>students</span>
                                </button>
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                    <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                  </svg>
                                  <span>2 months</span>
                                </button>
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                            </div>
                        </div>
                    </div>
                    <!-- end card buttons -->
                    <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                  </div>
              </div>
          </div>

          <div class="col-md-3">
              <div class="card custom-card">
                  <div class="card-body">
                    <!-- rectangle -->
                    <div class="Rectangle">
                      <div class="deadline">
                        <p>Closing soon April 24</p>
                    </div>
                  </div>
                    <!-- end rectangle -->
                    <h5 class="card-title">Project Name can put here</h5>
                    <p class="card-text">In publishing and graphic design,
                      Lorem ipsum is a placeholder text commonly used to
                      demonstrate the visual form of a document or a typeface
                      without relying on meaningful content.n publishing and
                      graphic design, Lorem ipsum is a placeholder text commonly
                       used to demonstrate.</p>
                      <!-- card buttons -->
                       <div class="button-container">
                        <div class="row">
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                    <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                  </svg>
                                  <span>students</span>
                                </button>
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                    <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                  </svg>
                                  <span>2 months</span>
                                </button>
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                            </div>
                        </div>
                    </div>
                    <!-- end card buttons -->
                    <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                  </div>
              </div>
          </div>

          <div class="col-md-3">
              <div class="card custom-card">
                  <div class="card-body">
                    <!-- rectangle -->
                    <div class="Rectangle">
                      <div class="deadline">
                        <p>Closing soon April 24</p>
                    </div>
                  </div>
                    <!-- end rectangle -->
                    <h5 class="card-title">Project Name can put here</h5>
                    <p class="card-text">In publishing and graphic design,
                      Lorem ipsum is a placeholder text commonly used to
                      demonstrate the visual form of a document or a typeface
                      without relying on meaningful content.n publishing and
                      graphic design, Lorem ipsum is a placeholder text commonly
                       used to demonstrate.</p>
                      <!-- card buttons -->
                       <div class="button-container">
                        <div class="row">
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                    <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                  </svg>
                                  <span>students</span>
                                </button>
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                    <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                  </svg>
                                  <span>2 months</span>
                                </button>
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                            </div>
                        </div>
                    </div>
                    <!-- end card buttons -->
                    <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                  </div>
              </div>
          </div>

          <div class="col-md-3">
              <div class="card custom-card">
                  <div class="card-body">
                    <!-- rectangle -->
                    <div class="Rectangle">
                      <div class="deadline">
                        <p>Closing soon April 24</p>
                    </div>
                  </div>
                    <!-- end rectangle -->
                    <h5 class="card-title">Project Name can put here</h5>
                    <p class="card-text">In publishing and graphic design,
                      Lorem ipsum is a placeholder text commonly used to
                      demonstrate the visual form of a document or a typeface
                      without relying on meaningful content.n publishing and
                      graphic design, Lorem ipsum is a placeholder text commonly
                       used to demonstrate.</p>
                      <!-- card buttons -->
                       <div class="button-container">
                        <div class="row">
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                    <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                  </svg>
                                  <span>students</span>
                                </button>
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                    <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                  </svg>
                                  <span>2 months</span>
                                </button>
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                            </div>
                        </div>
                    </div>
                    <!-- end card buttons -->
                    <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                  </div>
              </div>
          </div>
      </div>
      <!-- end row 2 -->

      <!-- row 3 -->
      <div class="row">
        <div class="col-md-3">
            <div class="card custom-card">
                <div class="card-body">
                  <!-- rectangle -->
                  <div class="Rectangle">
                    <div class="deadline">
                      <p>Closing soon April 24</p>
                  </div>
                </div>
                  <!-- end rectangle -->
                  <h5 class="card-title">Project Name can put here</h5>
                  <p class="card-text">In publishing and graphic design,
                    Lorem ipsum is a placeholder text commonly used to
                    demonstrate the visual form of a document or a typeface
                    without relying on meaningful content.n publishing and
                    graphic design, Lorem ipsum is a placeholder text commonly
                     used to demonstrate.</p>
                    <!-- card buttons -->
                     <div class="button-container">
                      <div class="row">
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                  <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                </svg>
                                <span>students</span>
                              </button>
                          </div>
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                  <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                  <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                </svg>
                                <span>2 months</span>
                              </button>
                          </div>
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                          </div>
                      </div>
                  </div>
                  <!-- end card buttons -->
                  <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card custom-card">
                <div class="card-body">
                  <!-- rectangle -->
                  <div class="Rectangle">
                    <div class="deadline">
                      <p>Closing soon April 24</p>
                  </div>
                </div>
                  <!-- end rectangle -->
                  <h5 class="card-title">Project Name can put here</h5>
                  <p class="card-text">In publishing and graphic design,
                    Lorem ipsum is a placeholder text commonly used to
                    demonstrate the visual form of a document or a typeface
                    without relying on meaningful content.n publishing and
                    graphic design, Lorem ipsum is a placeholder text commonly
                     used to demonstrate.</p>
                    <!-- card buttons -->
                     <div class="button-container">
                      <div class="row">
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                  <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                </svg>
                                <span>students</span>
                              </button>
                          </div>
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                  <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                  <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                </svg>
                                <span>2 months</span>
                              </button>
                          </div>
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                          </div>
                      </div>
                  </div>
                  <!-- end card buttons -->
                  <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card custom-card">
                <div class="card-body">
                  <!-- rectangle -->
                  <div class="Rectangle">
                    <div class="deadline">
                      <p>Closing soon April 24</p>
                  </div>
                </div>
                  <!-- end rectangle -->
                  <h5 class="card-title">Project Name can put here</h5>
                  <p class="card-text">In publishing and graphic design,
                    Lorem ipsum is a placeholder text commonly used to
                    demonstrate the visual form of a document or a typeface
                    without relying on meaningful content.n publishing and
                    graphic design, Lorem ipsum is a placeholder text commonly
                     used to demonstrate.</p>
                    <!-- card buttons -->
                     <div class="button-container">
                      <div class="row">
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                  <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                </svg>
                                <span>students</span>
                              </button>
                          </div>
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                  <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                  <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                </svg>
                                <span>2 months</span>
                              </button>
                          </div>
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                          </div>
                      </div>
                  </div>
                  <!-- end card buttons -->
                  <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card custom-card">
                <div class="card-body">
                  <!-- rectangle -->
                  <div class="Rectangle">
                    <div class="deadline">
                      <p>Closing soon April 24</p>
                  </div>
                </div>
                  <!-- end rectangle -->
                  <h5 class="card-title">Project Name can put here</h5>
                  <p class="card-text">In publishing and graphic design,
                    Lorem ipsum is a placeholder text commonly used to
                    demonstrate the visual form of a document or a typeface
                    without relying on meaningful content.n publishing and
                    graphic design, Lorem ipsum is a placeholder text commonly
                     used to demonstrate.</p>
                    <!-- card buttons -->
                     <div class="button-container">
                      <div class="row">
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                  <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                                </svg>
                                <span>students</span>
                              </button>
                          </div>
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-week" viewBox="0 0 16 16">
                                  <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                  <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                                </svg>
                                <span>2 months</span>
                              </button>
                          </div>
                          <div class="col-md-5">
                              <button type="button" class="btn btn-outline-primary"><span>Website</span></button>
                          </div>
                      </div>
                  </div>
                  <!-- end card buttons -->
                  <button type="button" class="btn btn-outline-secondary"><span>Apply Now</span></button>
                </div>
            </div>
        </div>
    </div>
    <!-- end row 3 -->

    </div>
    <!-- end card -->
    </div>

@endsection

