<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('general.Landing');
    }

    public function aboutUs(){
        return view('general.AboutUs');
    }

    public function contactUs(){
        return view('general.ContactUs');
    }

    public function completedprojects(){
        return view('general.completedProjects');
    }
}